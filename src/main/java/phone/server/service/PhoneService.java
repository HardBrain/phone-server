package phone.server.service;

import org.springframework.web.multipart.MultipartFile;
import phone.server.domain.PhoneNumbers;

import javax.xml.bind.Marshaller;

/**
 * @author Gleb Nogaj
 */

public interface PhoneService {

    void replenishNumbers(MultipartFile file);

}
