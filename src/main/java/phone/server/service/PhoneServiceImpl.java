package phone.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import phone.server.domain.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleb Nogaj
 */

@Service
public class PhoneServiceImpl implements PhoneService {

    private static final int MERCHANT_ID = 127541;
    private static final String MERCHANT_PASSWORD = "1S5819o6DkFntY8alePZ1VZDG67x2Vbs";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void replenishNumbers(MultipartFile inputFile) {

        try {

            PhoneNumbers numbers = getPhoneNumbersFromXmlFIle(inputFile);
            Data data = createDataWithNumbers(numbers);
            createDataXmlFile(data);

            String md5 = md5(getStringFromDataXmlWithPassword());

            Request request = new Request(1, createMerchant(sha1(md5)),data);
            createRequestXmlFile(request);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_XML);


            BufferedReader br = new BufferedReader(new FileReader(new File("request.xml")));
            String line;
            StringBuilder sb = new StringBuilder();

            while((line=br.readLine())!= null){
                sb.append(line.trim());
            }

            String requestBody = sb.toString();

            HttpEntity<String> entity = new HttpEntity<>(requestBody,headers);

            ResponseEntity<String> responseEntity =
                    restTemplate.postForEntity("https://api.privatbank.ua/p24api/directfill", entity, String.class);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;

            builder = factory.newDocumentBuilder();
            Document document = builder.parse( new InputSource( new StringReader( responseEntity.getBody() ) ) );

            System.out.println("------------------------------");
            printDocument(document,System.out);


        } catch (JAXBException
                | IOException
                | NoSuchAlgorithmException
                | TransformerException
                | ParserConfigurationException
                | SAXException e) {
            e.printStackTrace();
        }
    }

    private void createRequestXmlFile(Request request) throws JAXBException {
        File file = new File("request.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Request.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.marshal(request, file);

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(request, System.out);
    }

    private void createDataXmlFile(Data data) throws JAXBException {
        File dataFile = new File("data.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
        Marshaller jaxbMarshallerData = jaxbContext.createMarshaller();
        jaxbMarshallerData.marshal(data,dataFile);
    }

    private PhoneNumbers getPhoneNumbersFromXmlFIle(MultipartFile inputFile) throws IOException, JAXBException {
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(inputFile.getBytes());
        JAXBContext jaxbContext = JAXBContext.newInstance(PhoneNumbers.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (PhoneNumbers) jaxbUnmarshaller.unmarshal(arrayInputStream);
    }

    private String getStringFromDataXmlWithPassword() throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(new File("data.xml")));
        String line;
        StringBuilder sb = new StringBuilder();

        while((line=br.readLine())!= null){
            sb.append(line.trim());
        }

        String s = sb.toString();
        String data = s.substring(s.lastIndexOf("<oper>"), s.indexOf("</data>"));

        return data.concat(MERCHANT_PASSWORD);
    }

    private Data createDataWithNumbers(PhoneNumbers phoneNumbers) {

        Data data = new Data();
        data.setTest(1);
        data.setWait(0);
        data.setOper("cmt");
        data.setPayment(getPayments(phoneNumbers.getPhones()));

        return data;
    }

    private Merchant createMerchant(String signature){

        Merchant merchant = new Merchant();
        merchant.setId(MERCHANT_ID);
        merchant.setSignature(signature);

        return merchant;
    }

    private List<Payment> getPayments(List<String> phones) {

        List<Payment> payments = new ArrayList<>();

        for(int i = 0 ; i < phones.size(); i++){

            List<Prop> props = new ArrayList<>();
            props.add(new Prop("phone","%2B" + phones.get(i)));
            props.add(new Prop("amt","5.0"));

            payments.add(new Payment(String.valueOf(i),props));

        }

        return payments;
    }

    private String md5(String dataPassword) throws NoSuchAlgorithmException {

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        StringBuilder hexString = getHexString(dataPassword, messageDigest);

        return  hexString.toString();
    }

    private String sha1(String md5) throws NoSuchAlgorithmException {

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        StringBuilder hexString = getHexString(md5, messageDigest);

        return hexString.toString();
    }

    private StringBuilder getHexString(String md5, MessageDigest messageDigest) {

        messageDigest.update(md5.getBytes());
        byte byteData[] = messageDigest.digest();
        StringBuilder hexString = new StringBuilder();
        for (byte aByteData : byteData) {
            String hex = Integer.toHexString(0xff & aByteData);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString;
    }

    private static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        transformer.transform(new DOMSource(doc),
                new StreamResult(new OutputStreamWriter(out, "UTF-8")));
    }
}
