package phone.server.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Gleb Nogaj
 */
@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Request {

    private int version;
    private Merchant merchant;
    private Data data;

    public int getVersion() {
        return version;
    }

    @XmlAttribute
    public void setVersion(int version) {
        this.version = version;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
