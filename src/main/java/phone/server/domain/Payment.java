package phone.server.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.List;

/**
 * @author Gleb Nogaj
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Payment {

    private String id;
    private List<Prop> prop;

    public String getId() {
        return id;
    }

    @XmlAttribute
    public void setId(String id) {
        this.id = id;
    }

    public List<Prop> getProp() {
        return prop;
    }

    public void setProp(List<Prop> prop) {
        this.prop = prop;
    }
}
