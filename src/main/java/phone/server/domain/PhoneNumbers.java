package phone.server.domain;

import lombok.*;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Gleb Nogaj
 */
@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumbers {

    private Long id;
    private List<String> phones;

    public Long getId() {
        return id;
    }


    public List<String> getPhones() {
        return phones;
    }

    @XmlAttribute
    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }
}
