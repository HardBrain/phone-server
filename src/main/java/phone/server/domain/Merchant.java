package phone.server.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Gleb Nogaj
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Merchant {
    private int id;
    private String signature;
}
