package phone.server.web;

import lombok.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import phone.server.domain.PhoneNumbers;
import phone.server.service.PhoneService;

import javax.xml.bind.Marshaller;
import javax.xml.crypto.dsig.XMLObject;
import java.awt.*;

/**
 * @author Gleb Nogaj
 */
@RestController
@RequiredArgsConstructor
public class PhoneController {

    private final PhoneService phoneService;

    @PostMapping(value = "/upload")
    public void testString(@RequestBody MultipartFile multipartFile){
        phoneService.replenishNumbers(multipartFile);
    }

}
